#coding: utf-8
import json
import unidecode

caminho = "../data/brasil_estados.geojson"
saida_js = "../data/geodados.js"
saida_json = "../data/geodados.json"
cam_montantes = 'montantes.txt'
cam_csv = "../data/FalsificacaoDadosAbertos.csv"

def le_dados():
    with open(caminho) as arq:
        dados = json.loads(arq.read())
        arq.close()
    return dados

def gera_js(txt_json):
    preambulo = "var geodados = "
    texto = preambulo + txt_json
    with open(saida_js, 'w') as arq:
        arq.write(texto)
        arq.close()

def gera_json(txt_json):
    txt_json = json.loads(txt_json)
    with open(saida_json, 'w') as arq:
        json.dump(txt_json, arq, indent=4)
        arq.close()


estadoAno={}
#estadoAno["CEARA"]={"ano":2010,"total":0}
def makeResource(linha):
    ano, estado,void, valor, qtd = linha.split(',')
    valor = float(valor)
    qtd = int(qtd.strip())
    total = valor*qtd
    estado = unidecode.unidecode(estado)

    try:
        x = estadoAno[estado]
    except:
        estadoAno[estado]={}
    try:
        x = estadoAno[estado][ano]
    except:
        estadoAno[estado][ano]=0

    estadoAno[estado][ano] += total


with open(cam_csv) as arq:
    for i in arq.readlines()[1:]:
        makeResource(i)


dados = le_dados()
dicIdNomeVal = {}

with open(cam_montantes) as arq:
    for i in arq:
        # print(i.split('\t'))
        iD, nome, valor = i.split('\t')
        valor = int(valor.strip())
        dicIdNomeVal[iD] = (nome, valor)
    arq.close()

for i in range(27):
    num_uf = str(dados['features'][i]['properties']['cartodb_id'])
    nome_uf = dicIdNomeVal[num_uf][0]

    dados['features'][i]['properties']['nome_uf'] = dicIdNomeVal[num_uf][0]
    dados['features'][i]['properties']['montante'] = dicIdNomeVal[num_uf][1]
    dados['features'][i]['properties']['valores'] = estadoAno[nome_uf]
    try:
        dados['features'][i]['properties'].__delitem__('simounao')
    except:
        pass

    # print("\n>>", num_uf, lista[num_uf])
    # print(num_uf, dados['features'][i]['properties']['nome_uf'], end='\t')
    # print(dados['features'][i]['properties']['uf_05'])
    # print(dados['features'][i]['properties'])

json_saida = json.dumps(dados)
print(json_saida)
gera_json(json_saida)